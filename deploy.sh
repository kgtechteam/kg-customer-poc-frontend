#!/bin/bash

CONTAINER_PORT="8081"
HOST_PORT="80"

printf "Please enter docker image name [kg-crm-poc-frontend]: "
read -r IMAGE_NAME

printf "Please enter image tag [prod]: "
read -r IMAGE_TAG

docker build -t "${IMAGE_NAME}":"${IMAGE_TAG}" .

printf "Build completed... \n\n"
sleep 1;
printf "Starting run script... \n"
sleep 1;

sh ./run.sh "${IMAGE_NAME}" "${IMAGE_TAG}" ${HOST_PORT} ${CONTAINER_PORT}
