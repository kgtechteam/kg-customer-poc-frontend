export const schema = {
  'definitions': {},
  '$schema': 'http://json-schema.org/draft-07/schema#',
  '$id': 'http://example.com/root.json',
  'type': 'object',
  'title': 'The Root Schema',
  'required': [
    'id',
    'instanceId',
    'methodName',
    'request',
    'response',
    'status',
    'time'
  ],
  'properties': {
    'id': {
      '$id': '#/properties/id',
      'type': 'string',
      'title': 'The Id Schema',
      'default': '',
      'examples': [
        '5e279694f0be725a076bb848'
      ],
      'pattern': '^(.*)$'
    },
    'instanceId': {
      '$id': '#/properties/instanceId',
      'type': 'string',
      'title': 'The Instanceid Schema',
      'default': '',
      'examples': [
        'customer-service-27a203fa-69f6-4b07-9611-642801075e02'
      ],
      'pattern': '^(.*)$'
    },
    'methodName': {
      '$id': '#/properties/methodName',
      'type': 'string',
      'title': 'The Methodname Schema',
      'default': '',
      'examples': [
        'customers'
      ],
      'pattern': '^(.*)$'
    },
    'request': {
      '$id': '#/properties/request',
      'type': 'string',
      'title': 'The Request Schema',
      'default': '',
      'examples': [
        '[com.querydsl.core.BooleanBuilder@0, Page request [number: 0, size 20, sort: UNSORTED]]'
      ],
      'pattern': '^(.*)$'
    },
    'response': {
      '$id': '#/properties/response',
      'type': 'string',
      'title': 'The Response Schema',
      'default': '',
      'examples': [
        'Page 1 of 1 containing com.poc.customerservice.domain.Customer instances'
      ],
      'pattern': '^(.*)$'
    },
    'status': {
      '$id': '#/properties/status',
      'type': 'string',
      'title': 'The Status Schema',
      'default': '',
      'examples': [
        'SUCCESS'
      ],
      'pattern': '^(.*)$'
    },
    'time': {
      '$id': '#/properties/time',
      'type': 'string',
      'title': 'The Time Schema',
      'default': '',
      'examples': [
        '2020-01-22T03:25:56.139+03:00'
      ],
      'pattern': '^(.*)$'
    }
  }
};
