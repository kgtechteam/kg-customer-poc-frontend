import {BaseService} from '../../shared/services/base/base.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {LogModel} from '../../model/log.model';
import {HttpParams} from '@angular/common/http';
import {PaginationModel} from '../../model/page.model';

@Injectable({
  providedIn: 'root'
})
export class LogService extends BaseService {
  private resourceUrl = 'logger-service/api/logs';

  size = 1;
  page = 10;

  getPath(): string {
    return this.resourceUrl;
  }

  getAllLogs(): Observable<PaginationModel<Array<LogModel>>> {
    return this.get<PaginationModel<Array<LogModel>>>();
  }

  searchLog(log: LogModel): Observable<any> {
    let searchParams = '';

    Object.keys(log).forEach((key, index) => {
      if (log[key]) {
        searchParams += key + '=' + log[key] + '&';
      }
    });

    return this.get<any>(searchParams ? '?' + searchParams.substring(0, searchParams.length - 1) : '');
  }

  getHttpParams(): HttpParams {
    return new HttpParams().append('size', `${this.size}`).append('page', `${this.page - 1}`);
  }
}
