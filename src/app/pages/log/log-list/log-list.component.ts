import {Component, OnInit} from '@angular/core';
import {LogService} from '../log.service';
import {LogModel} from '../../../model/log.model';
import {PopupService} from '../../../shared/services/popup.service';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {JsonEditorOptions} from 'ang-jsoneditor';
import {NzModalService} from 'ng-zorro-antd';
import {schema} from '../schema.value';
import {PaginationModel} from '../../../model/page.model';

@Component({
  selector: 'kg-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.scss']
})
export class LogListComponent implements OnInit {
  logs: LogModel[] = [];
  options = new JsonEditorOptions();
  isVisible = false;
  findLog: any;
  loading = false;
  total = 0;
  page = 1;
  size = 10;

  logSearchForm = this.formBuilder.group({
    id: new FormControl(''),
    methodName: new FormControl(''),
    instanceId: new FormControl(''),
    status: new FormControl(''),
  }, {updateOn: 'blur'});

  constructor(
    private formBuilder: FormBuilder,
    private logService: LogService,
    private popupService: PopupService,
    private modalService: NzModalService) {
    this.options.mode = 'code';
    this.options.modes = ['code', 'text', 'tree', 'view'];
    this.options.schema = schema;
  }

  ngOnInit() {
    this.getAllLogs();
  }

  getAllLogs(reset: boolean = false) {
    if (reset) {
      this.page = 1;
    }
    this.loading = true;
    this.logService.page = this.page;
    this.logService.size = this.size;
    this.logService.getAllLogs().subscribe(logPage => {
      this.loading = false;
      this.total = logPage.totalElements;
      this.logs = logPage.content;
    });
  }

  searchLog() {
    this.logService.searchLog(
      {
        id: this.logSearchForm.get('id').value,
        methodName: this.logSearchForm.get('methodName').value,
        instanceId: this.logSearchForm.get('instanceId').value,
        status: this.logSearchForm.get('status').value
      }
    ).subscribe(logPage => {
      this.logs = logPage.content;
    });
  }

  reset() {
    this.getAllLogs();
  }

  showModal(log: LogModel): void {
    log = this.logs.find(l => l.id === log.id);
    this.findLog = log;
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
