import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LogListComponent} from './log-list/log-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedNgZorroModule} from '../../shared';
import {NgJsonEditorModule} from 'ang-jsoneditor';

const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: LogListComponent},
];

@NgModule({
  declarations: [LogListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    SharedNgZorroModule,
    FormsModule,
    NgJsonEditorModule
  ]
})

export class LogModule {

}
