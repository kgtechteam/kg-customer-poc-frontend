import {BaseService} from '../../shared/services/base/base.service';
import {Observable} from 'rxjs';
import {CustomerModel} from '../../model/customer.model';
import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';
import {PaginationModel} from '../../model/page.model';
import {LogModel} from '../../model/log.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends BaseService {
  private resourceUrl = 'customer-service/api/customer';

  size = 1;
  page = 10;

  getPath(): string {
    return this.resourceUrl;
  }

  createCustomerRequest(body: any): Observable<CustomerModel> {
    return this.post<CustomerModel>(body);
  }

  getAllCustomer(): Observable<PaginationModel<Array<CustomerModel>>> {
    return this.get<PaginationModel<Array<CustomerModel>>>();
  }

  getCustomerOfTckn(tckn: string): Observable<any> {
    return this.get<any>('?tckn=' + tckn);
  }

  updateCustomer(customerModel: CustomerModel): Observable<any> {
    return this.put<any>('', customerModel);
  }

  getHttpParams(): HttpParams {
    return new HttpParams().append('size', `${this.size}`).append('page', `${this.page - 1}`);
  }
}
