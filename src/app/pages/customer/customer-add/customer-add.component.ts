import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {CustomerModel} from '../../../model/customer.model';
import {CustomerService} from '../customer.service';
import {NzNotificationService} from 'ng-zorro-antd';
import {AddressModel} from '../../../model/address.model';

@Component({
  selector: 'kg-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit {

  customerModel = new CustomerModel();
  addresses = new Array<AddressModel>();
  selectedAddressModel = new AddressModel();

  customerFormGroup = this.formBuilder.group({
    tc: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    birthYear: new FormControl('', Validators.required),
    mail: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    postCode: new FormControl('', Validators.required)
  }, {updateOn: 'blur'});

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private customerService: CustomerService,
              private toastrService: NzNotificationService) {
  }

  ngOnInit() {
  }

  addCustomer() {

    const {
      tc,
      name,
      surname,
      birthYear,
      mail,
      phone,
      postCode,
      country,
      city,
      street
    } = this.customerFormGroup.value;
    this.customerModel.tckn = Number(tc);
    this.customerModel.name = name;
    this.customerModel.surname = surname;
    this.customerModel.birthYear = Number(birthYear);
    this.customerModel.email = mail;
    this.customerModel.phone = phone;
    this.customerModel.address.postCode = postCode;
    this.customerModel.address.country = country;
    this.customerModel.address.city = city;
    this.customerModel.address.street = street;

    if (!this.customerFormGroup.invalid) {
      this.customerService.createCustomerRequest(this.customerModel).subscribe(result => {
        this.toastrService.success('Successful', 'Registration successful');
        setTimeout(() => {
          this.router.navigate(['customer/list']);
        }, 100);
      }, error => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }
}
