import {Component, OnInit} from '@angular/core';
import {CustomerModel} from '../../../model/customer.model';
import {Router} from '@angular/router';
import {CustomerService} from '../customer.service';

@Component({
  selector: 'kg-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  customers: CustomerModel[] = [];
  loading = false;
  total = 0;
  page = 1;
  size = 10;

  constructor(
    private customerService: CustomerService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.searchData();
  }

  editCustomer(tckn: number) {
    this.router.navigate([`customer/edit/${tckn}`]);
  }

  addCustomer() {
    this.router.navigate(['customer/add']);
  }

  searchData(reset: boolean = false): void {
    if (reset) {
      this.page = 1;
    }
    this.loading = true;
    this.customerService.page = this.page;
    this.customerService.size = this.size;
    this.customerService.getAllCustomer().subscribe(customer => {
      this.loading = false;
      this.total = customer.totalElements;
      this.customers = customer.content;
    });
  }

}
