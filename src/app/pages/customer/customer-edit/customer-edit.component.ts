import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../customer.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {CustomerModel} from '../../../model/customer.model';
import {AddressModel} from '../../../model/address.model';

@Component({
  selector: 'kg-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.scss']
})
export class CustomerEditComponent implements OnInit {

  customerUrlName: string;
  editCustomerInformationModel = new CustomerModel();
  updateCustomerInformationModel = new CustomerModel();

  customerFormGroup = this.formBuilder.group({
    tc: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    birthYear: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    postCode: new FormControl('', Validators.required)
  }, {updateOn: 'blur'});

  constructor(private customerService: CustomerService,
              private route: ActivatedRoute,
              private toastrService: NzNotificationService,
              private formBuilder: FormBuilder,
              private router: Router
  ) {
    this.customerUrlName = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.customerService.getCustomerOfTckn(this.customerUrlName).subscribe(result => {
      if (result.content) {
        this.editCustomerInformationModel = result.content[0];
        this.fillingInCustomerInformationToBeEditedInTheForm();
      }
    });
  }

  fillingInCustomerInformationToBeEditedInTheForm() {
    this.customerFormGroup.patchValue({
      tc: this.customerUrlName,
      name: this.editCustomerInformationModel.name,
      surname: this.editCustomerInformationModel.surname,
      birthYear: this.editCustomerInformationModel.birthYear,
      email: this.editCustomerInformationModel.email,
      phone: this.editCustomerInformationModel.phone,
      postCode: this.editCustomerInformationModel.address.postCode,
      country: this.editCustomerInformationModel.address.country,
      city: this.editCustomerInformationModel.address.city,
      street: this.editCustomerInformationModel.address.street
    });
  }

  save() {
    this.updateCustomerInformationModel.tckn = Number(this.customerFormGroup.get('tc').value);
    this.updateCustomerInformationModel.name = this.customerFormGroup.get('name').value;
    this.updateCustomerInformationModel.surname = this.customerFormGroup.get('surname').value;
    this.updateCustomerInformationModel.birthYear = Number(this.customerFormGroup.get('birthYear').value);
    this.updateCustomerInformationModel.email = this.customerFormGroup.get('email').value;
    this.updateCustomerInformationModel.phone = this.customerFormGroup.get('phone').value;
    this.updateCustomerInformationModel.address.postCode = this.customerFormGroup.get('postCode').value;
    this.updateCustomerInformationModel.address.country =  this.customerFormGroup.get('country').value;
    this.updateCustomerInformationModel.address.city = this.customerFormGroup.get('city').value;
    this.updateCustomerInformationModel.address.street = this.customerFormGroup.get('street').value;

    if (!this.customerFormGroup.invalid) {
      this.customerService.updateCustomer(this.updateCustomerInformationModel).subscribe(result => {
        this.toastrService.success('Successful', 'Registration successful');
        setTimeout(() => {
          this.router.navigate(['customer/list']);
        }, 100);
      }, error => {
        this.toastrService.error('Error', 'Registration failed');
      });
    } else {
      this.toastrService.warning('Warning', 'Fill in the required fields marked with an asterisk');
    }
  }
}
