import { Injectable } from '@angular/core';
import {NzModalService} from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(private modalService: NzModalService) {
  }

  successPopup(data: any) {
    this.modalService.success({
      nzTitle: data.id,
      nzContent: '<json-editor [options]="options" [data]="data"></json-editor>',
      nzOkText: 'Ok'
    });
  }

  errorPopup(data: any) {
    this.modalService.error({
      nzTitle: data.id,
      nzContent: JSON.stringify(data),
      nzOkText: 'Ok'
    });
  }
}
