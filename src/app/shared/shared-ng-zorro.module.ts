import { NgModule } from '@angular/core';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import {IconsProviderModule} from './icons-provider.module';

@NgModule({
  imports: [
    IconsProviderModule,
    NgZorroAntdModule,
  ],
  exports: [
    IconsProviderModule,
    NgZorroAntdModule,
  ]
})
export class SharedNgZorroModule { }
