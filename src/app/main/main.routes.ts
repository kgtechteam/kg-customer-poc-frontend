import {Routes} from '@angular/router';

/**
 * This file contains all sub modules and components.
 */

export const mainRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'customer'
  },
  {
    path: 'customer',
    loadChildren: () => import('../pages/customer/customer.module').then(m => m.CustomerModule)
  },
  {
    path: 'log',
    loadChildren: () => import('../pages/log/log.module').then(m => m.LogModule)
  }
];
