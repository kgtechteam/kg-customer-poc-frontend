export class AddressModel {
  city?: string;
  country?: string;
  street?: string;
  postCode?: number;
}
