export class LogModel {
  id?: string;
  methodName?: string;
  request?: any;
  response?: any;
  instanceId?: string;
  status?: string;
  time?: string;
}
