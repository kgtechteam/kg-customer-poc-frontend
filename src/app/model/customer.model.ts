import {AddressModel} from './address.model';

export class CustomerModel {
  tckn?: number;
  name?: string;
  surname?: string;
  birthYear?: number;
  email?: string;
  phone?: string;
  address = new AddressModel();
}
